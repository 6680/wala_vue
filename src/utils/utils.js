
 /**
  * 2020.12.28
  * 韩瑞丰：统一抽离
  */

 function browserView(){
    //判断访问终端
    var browser={
        versions:function(){
            var u = navigator.userAgent, 
            app = navigator.appVersion;
            return {
                trident: u.indexOf('Trident') > -1, //IE内核
                presto: u.indexOf('Presto') > -1, //opera内核
                webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1,//火狐内核
                mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
                ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端
                iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
                iPad: u.indexOf('iPad') > -1, //是否iPad
                webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
                weixin: u.indexOf('MicroMessenger') > -1, //是否微信
                qq: u.match(/\sQQ/i) == " qq" //是否QQ
            };
        }()
    
    }
    return browser
    }
    
    export function dreveView(){
        if(browserView.versions.mobile){
            console.log("移动端")
            //判断机型是安卓或IOS
            if(browserView.versions.android) {
                return "android"
            }else if(browserView.versions.ios){
                return "ios"
            }
        }
    }
    /**
     * 时间格式化处理
     * 时间戳转化日期（时分秒）
     * 实例：var timestamp = new Date().getTime(); //当前时间戳
    　  timestampToTime(timestamp)
     */
    export function formatDate(timestamp){
            var date = new Date();//时间戳为10位需*1000，时间戳为13位的话不需乘1000
            var Y = date.getFullYear() + '-';
            var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
            var D = (date.getDate() < 10 ? '0'+date.getDate() : date.getDate()) + ' ';
            var h = (date.getHours() < 10 ? '0'+date.getHours() : date.getHours()) + ':';
            var m = (date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes()) + ':';
            var s = (date.getSeconds() < 10 ? '0'+date.getSeconds() : date.getSeconds());
            
            let strDate = Y+M+D+h+m+s;
            return strDate;      
    }
    //将日期转换为时间戳
    export function timestampView(date){
        var d = new Date();
        return d.getTime()
    }
    

    /**
			 * 获取参数
			 */
		export	 function getQueryString(name) {
            let urlserch = new URLSearchParams(window.location.href)
            if(urlserch.get(name)){
                return urlserch.get(name)
            }
            return "";
			
			}