/**
 * 韩瑞丰
 * 统一网络请求封装
 * Date:2021.1.19
 * 
 */
/**
 * axios封装
 * 请求拦截、响应拦截、错误统一处理
 */
/* 做数据请求封装 */
import axios from 'axios'
import bus from "@/utils/bus"
import {getQueryString} from "@/utils/utils"
//判断  master or dev
 var dev_img = "https://image.fengbaotek.com/image/"
 var master_img = "https://image.fengbaotek.com/image/"

export var imgPath =""
export var uid = getQueryString('user_id');
export var secret = getQueryString('secret');
//判断开发环境
if (process.env.NODE_ENV == 'development') {    
  imgPath = dev_img
    axios.defaults.baseURL = 'https://dev-server.wawalala.top';} 
else if (process.env.NODE_ENV == 'debug') {    
  imgPath = dev_img
    axios.defaults.baseURL = 'https://dev-server.wawalala.top';
} 
else if (process.env.NODE_ENV == 'production') {    
  imgPath = master_img
    axios.defaults.baseURL = 'https://server.wawalala.top';
}


// 1. axios默认配置
// axios.defaults.baseURL = 'http://localhost:3000';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
/*
*post 请求方法
* @param url
* @param data
* @returns {Promise}
 */
export  function post(url, data = {}) {
  return new Promise((resolve, reject) => {
    axios.post(url, data)
      .then(response => {
        resolve(response.data)
      }).catch(err => {
      reject(err)
    })
  })
}
/*
*get 请求方法
* @param url
* @param data
* @returns {Promise}
 */
export function get(ctx,url, params = {}) {
  ctx.$emit("logd",true)
  return new Promise((resolve, reject) => {
    axios.get(url, {
      params: params
    })
      .then(response => {
       
        ctx.$nextTick(()=>{
          
           ctx.$bus.$emit("logd",false)
        })
        resolve(response.data)
      }).catch(err => {
        ctx.$bus.$emit("logd",false)
      reject(err)
    })
  })
}
/*
*patch 请求方法
* @param url
* @param data
* @returns {Promise}
 */
export function patch(url, data = {}) {
  return new Promise((resolve, reject) => {
    axios.patch(url, data)
      .then(response => {
        resolve(response.data)
      }).catch(err => {
      reject(err)
    })
  })
}
/*
*put 请求方法
* @param url
* @param data
* @returns {Promise}
 */
export function put(url, data = {}) {
  return new Promise((resolve, reject) => {
    axios.put(url, data)
      .then(response => {
        resolve(response.data)
      }).catch(err => {
      reject(err)
    })
  })
}

