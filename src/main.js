// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
const Bus = new Vue()
Vue.prototype.$bus = Bus

Vue.config.productionTip = false

/* eslint-disable no-new */

　　Vue.directive('title',{

  　　　　inserted:function(el){
  
  　　　　　　document.title = el.dataset.title
  
  　　　　}　　
  
  　　})
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
