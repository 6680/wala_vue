import Vue from 'vue'
import Router from 'vue-router'
import clanLevelRewards from '@/view/ar/clanLevelReward'
import grade_new from "@/view/ar/grade_new"
import cp from "@/view/ar/cp"
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/clanLevelRewards',
      component: clanLevelRewards
    },
    {
      path: '/grade_new',
      component: grade_new
    },
    {
      path: '/cp',
      component: cp
    }
  ]
})
